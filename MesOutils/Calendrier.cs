﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.AFCEPF.IntroCSharp.MesOutils
{
    public class Calendrier
    {
        public bool EstBisextile (int annee)
        {
            return ((annee % 4 == 0)&& (annee%100!=0)) || (annee%400==0);
        }
    }
}
