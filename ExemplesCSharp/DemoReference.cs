﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExemplesCSharp
{
    class DemoReference
    {
        public void Tester()
        {
            Console.WriteLine("Veuillez saisir un nombre");
           // int x = int.Parse(Console.ReadLine());
            string s = Console.ReadLine();
            //x = int.Parse(s);
            int x;
            int.TryParse(s, out x);
            Doubler(ref x);
            Console.WriteLine("Le double de x vaut : " + x);
        }
    public void Doubler(ref int x)
    {
            x *= 2;
    }

    }
}
