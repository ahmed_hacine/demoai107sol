﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.AFCEPF.IntroCSharp.MesOutils;

namespace Fr.AFCEPF.IntroCSharp.Tournoi
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1- ajouter une refernce à l'assembly (projet) désiré
            //2- Verifier que la classe à utiliser est bien public
            // 3- Ajouter une directuve using sur le nameSpace à utiliser
            
            Calendrier c = new Calendrier();
            int annee = 2024;
            Console.WriteLine("L'année {0} {1} bissextile", annee, (c.EstBisextile(annee)) ? "est" : "n'est pas");
            Console.ReadLine();

        }
    }
}
