﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Ville
    {
        public string Nom { get; set; }
        public int Population { get; set; }
        public Departement Departement { get; set; }
        public double Superficie { get; set; }
        public double Densite { get => Population / Superficie; }

        public Ville(string nom, int population, Departement departement, double superficie)
        {
            Nom = nom;
            Population = population;
            Departement = departement;
            Superficie = superficie;
        }

        public Ville()
        {
        }

        
    }
}
