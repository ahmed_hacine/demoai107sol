﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Departement
    {
        public string Nom { get; set; }
        public string Numero { get; set; }

        public Departement()
        {
        }

        public Departement(string nom, string numero)
        {
            Nom = nom;
            Numero = numero;
        }
    }
}
